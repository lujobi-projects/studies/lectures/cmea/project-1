#include <Eigen/Core>
#include <vector>
#include <iostream>
#include "writer.hpp"

/// Uses the Heun's method to compute u from time 0 to time T
/// for the ODE $u'=e^{-2t}-2u$
///
/// @param[out] u at the end of the call (solution at all time steps)
/// @param[out] time contains the time levels
/// @param[in] u0 the initial data
/// @param[in] dt the step size
/// @param[in] T the final time up to which to compute the solution.
///

void Heun(std::vector<double> & u, std::vector<double> & time,
          const double & u0, double dt, double T) {
    const unsigned int nsteps = round(T/dt);

    // LEAVING THE FUNCTION COMPLETELY UNTOUCHED
    // Set the vectors to correct size such that we don't need to use push_back several times
    u.resize(nsteps + 1);
    time.resize(nsteps + 1);

    // Since we are not allowed to make own functions, lambda function it is. -> more debug-friendly
    auto func = [](double t, double u){ return std::pow( M_E, -2*t) -2 * u; };

    // Allocate memory just once
    double t_curr = 0;
    double y_1 = 0;
    double y_2 = 0;

    // prevent one at() call to the vector
    double u_last = u0;

    for(int i = 0; i <= nsteps; ++i){
        t_curr = i * dt;

        y_1 = u_last;
        y_2 = u_last + dt * func(t_curr + 0.5 * dt, y_1);

        u_last = u_last + 0.5 * dt * (func(t_curr, y_1) + func(t_curr + 0.5 * dt, y_2));

        // Push to vectors
        time.at(i) = t_curr;
        u.at(i) = u_last;
    }
}

int main(int argc, char** argv) {

    auto func_exact = [](double u_0, double t){return (t+u_0)*std::exp(-2 * t);};

    // Initalizing constants
    double const T = 10.0;
    double const u_0 = 0.0;

    // double initialize dt just once
    double dt = 0;

    // Initialize Vectors to save errors and send to Heun function
    std::vector<double> time;
    std::vector<double> u;
    std::vector<double> errors;
    std::vector<double> dts;

    double u_t_exact = func_exact(u_0, T);

    for (int k = 1; k <= 8; ++k) {
        // recalculate dt
        dt = std::pow(2.0, -k);
        Heun(u, time, u_0, dt, T);
        // Prepare for plotting
        dts.push_back(dt);
        errors.push_back(std::abs(u[u.size() - 1] - u_t_exact));
    }

    /// Saving to files
    writeToFile("error.txt", errors);
    writeToFile("dt.txt", dts);

    return 0;
}
