#include <Eigen/Core>
#include <vector>
#include <iostream>
#include "writer.hpp"

/// Uses the Heun's method to compute u from time 0 to time T 
/// for the ODE $u'=e^{-2t}-2u$
///
/// @param[out] u at the end of the call (solution at all time steps)
/// @param[out] time contains the time levels
/// @param[in] u0 the initial data
/// @param[in] dt the step size
/// @param[in] T the final time up to which to compute the solution.
///

void Heun(std::vector<double> & u, std::vector<double> & time,
        const double & u0, double dt, double T) {
    const unsigned int nsteps = round(T/dt);

    // Set the vectors to correct size such that we don't need to use push_back several times
    u.resize(nsteps + 1);
    time.resize(nsteps + 1);

    // Since we are not allowed to make own functions, lambda function it is. -> more debug-friendly
    auto func = [](double t, double u){ return std::pow( M_E, -2*t) -2 * u; };

    // Allocate memory just once
    double t_curr = 0;
    double y_1 = 0;
    double y_2 = 0;

    // prevent one at() call to the vector
    double u_last = u0;

    for(int i = 0; i <= nsteps; ++i){
        t_curr = i * dt;

        y_1 = u_last;
        y_2 = u_last + dt * func(t_curr + 0.5 * dt, y_1);

        u_last = u_last + 0.5 * dt * (func(t_curr, y_1) + func(t_curr + 0.5 * dt, y_2));

        // Push to vectors
        time.at(i) = t_curr;
        u.at(i) = u_last;
    }
}

int main(int argc, char** argv) {

    double T = 10.0;

    double dt = 0.2;

    // To make some plotting easier, we take the dt parameter in as an optional
    // parameter.
    if (argc == 2) {
        dt = atof(argv[1]);
    }

    const double u0 = 0.;
    std::vector<double> time;
    std::vector<double> u;
    Heun(u,time,u0,dt,T);

    writeToFile("solution.txt", u);
    writeToFile("time.txt",time);

    return 0;
}
