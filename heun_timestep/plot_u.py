from numpy import *
import matplotlib
import matplotlib.pyplot as plt

Folder = "build/"

u = loadtxt(Folder + 'solution.txt')
t = loadtxt(Folder + 'time.txt')

plt.plot(t, u, label='$u(t)$')
plt.xlabel('$t$')
plt.ylabel('$u(t)$')
plt.legend(loc="best")
plt.show()
