#pragma once
#include <Eigen/Core>
#include <vector>
#include "zombieoutbreak.hpp"

class DIRKSolver {
public:
    DIRKSolver(const ZombieOutbreak& zombieOutbreak_)
        : zombieOutbreak(zombieOutbreak_) {

    }

    ///
    /// Evaluates function G1(y) from task b)
    /// @param[out] G evaluation of function
    /// @param[in] y input to G
    /// @param[in] tn current time
    /// @param[in] Un computed value of U at time tn
    /// @param[in] dt timestep
    ///
    //----------------G1Start----------------
    void computeG1(Eigen::Vector3d& G, Eigen::Vector3d y, double tn,
        Eigen::Vector3d Un, double dt) {
        Eigen::Vector3d f;
        zombieOutbreak.computeF(f, tn + mu * dt, y);
        G = Un + dt * mu * f - y;
    }
    //----------------G1End----------------

    ///
    /// Evaluates function G2(y1, y) from task b)
    /// @param[out] G evaluation of function
    /// @param[in] y input to G
    /// @param[in] tn current time
    /// @param[in] Un computed value of U at time tn
    /// @param[in] dt timestep
    /// @param[in] y1 computed value for first intermediate RK value
    ///
    void computeG2(Eigen::Vector3d& G, Eigen::Vector3d y, double tn,
        Eigen::Vector3d Un, double dt, Eigen::Vector3d y1) {

        Eigen::Vector3d f1;
        Eigen::Vector3d f2;

        zombieOutbreak.computeF(f1, tn + mu * dt, y1);
        zombieOutbreak.computeF(f2, tn + (1.0-mu) * dt, y);

        G = Un + dt * (-nu * f1 + mu * f2) - y;
    }

    ///
    /// Find a solution to JG1*x = -G1 with Newton's method
    /// @param[out] u solution to the system
    /// @param[in] Un computed value of U at time tn
    /// @param[in] dt timestep
    /// @param[in] tn current time
    /// @param[in] tolerance if Newton increment smaller, successfully converged
    /// @param[in] maxIterations  max Newton iterations to try before failing
    ///
    void newtonSolveY1(Eigen::Vector3d& u, Eigen::Vector3d Un, double dt,
        double tn, double tolerance, int maxIterations) {

        Eigen::Vector3d RHSG1, Fu, x;
        Eigen::Matrix3d JG1, JFu;
        u = Un;
	
        // Write your loop for the Newton solver here.
        // You will need to use zombieOutbreak.computeF(...)
        // and zombieOutbreak.computeJF(...)

        computeG1(RHSG1, u, tn, Un, dt);

        for (int i = 0; i < maxIterations; ++i) {
            zombieOutbreak.computeJF(JFu, tn + mu * dt, u);
            // Jacobian of G1
            JG1 = dt*mu*JFu - Eigen::Matrix3d::Identity();
            x = JG1.fullPivLu().solve(-RHSG1);

            u = u + x;

            computeG1(RHSG1, u, tn, Un, dt);

            if (RHSG1.norm() <= tolerance)
                return;
        }
        // If we reach this point, something wrong happened.

        throw std::runtime_error("Did not reach tolerance in Newton iteration");
    }

    ///
    /// Find a solution to JG2*x = -G2 with Newton's method
    /// @param[out] x solution to the system
    /// @param[in] Un computed value of U at time tn
    /// @param[in] y1 previous intermediate value for RK method
    /// @param[in] dt timestep
    /// @param[in] tn current time
    /// @param[in] tolerance if Newton increment smaller, successfully converged
    /// @param[in] maxIterations  max Newton iterations to try before failing
    ///
    //----------------NewtonG2Start----------------
    void newtonSolveY2(Eigen::Vector3d& v, Eigen::Vector3d Un,
        Eigen::Vector3d y1, double dt, double tn, double tolerance, int maxIterations) {

        Eigen::Vector3d RHSG2;
        Eigen::Matrix3d JG2, JF;
        v = Un;

        computeG2(RHSG2, v, tn, Un, dt, y1);

        for (int i = 0; i < maxIterations; ++i) {
            zombieOutbreak.computeJF(JF, tn + (1.0-mu) * dt, v);
            // Jacobian of G2
            JG2 = dt * mu * JF - Eigen::Matrix3d::Identity();
            v += JG2.fullPivLu().solve(-RHSG2);

            computeG2(RHSG2, v, tn, Un, dt, y1);

            if (RHSG2.norm() <= tolerance)
                return;
        }
        // If we reach this point, something wrong happened.
        throw std::runtime_error("Did not reach tolerance for RHSG2 in Newton iteration");

    }
    //----------------NewtonG2End----------------



    ///
    /// Compute N timesteps of DIRK(2,3)
    /// @param[in/out] u should be a vector of size 3, where each
    ///                component is a vector of size N+1. u[i][0]
    ///                should have the initial value stored before
    ///                calling the funtion
    ///
    /// @param[out] time should be of length N+1
    ///
    /// @param[in] T the final time
    /// @param[in] N the number of timesteps
    /// @param[in] k1 the constant k1
    /// @param[in] k2 the constant k2
    ///
    //----------------DirkStart----------------
    void solve(std::vector<std::vector<double> >& u, std::vector<double>& time,
        double T, int N) {

        const double dt = T / N;

        // Your main loop goes here. At iteration n,
        // 1) Find Y_1 with newtonSolveY1 (resp. Y2)
        // 2) Compute U^{n+1} with F(Y1), F(Y2)
        // 3) Write the values at u[...][n] and time[n]

        const double tol = 1e-15;
        const double max_iter = 10000;

        Eigen::Vector3d y1, y2, Un, f1, f2;

        Un << u[0][0], u[1][0], u[2][0];

        for (int i = 1; i <= N; ++i) {
            double t = i * dt;
            newtonSolveY1(y1, Un, dt, t, tol, max_iter);
            newtonSolveY2(y2, Un, y1, dt, t, tol, max_iter);
            zombieOutbreak.computeF(f1, t + mu*dt, y1);
            zombieOutbreak.computeF(f2, t + (1.0 - mu)*dt, y2);
            Un = Un + dt/2.0*(f1 + f2);

            u[0][i] = Un(0);
            u[1][i] = Un(1);
            u[2][i] = Un(2);
            time[i] = t;
        }

        // error: 0.26955453421604147
        // error: 0.26955424151674379

    }
    //----------------DirkEnd----------------

private:

    const double mu = 0.5 + 0.5 / sqrt(3);
    const double nu = 1. / sqrt(3);

    ZombieOutbreak zombieOutbreak;

}; // end class DIRKSolver
