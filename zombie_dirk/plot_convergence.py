from numpy import *
import matplotlib
import matplotlib.pyplot as plt
import sys

Folder = 'cmake-build-debug/'

error = loadtxt(Folder + 'errors.txt')
numbers = loadtxt(Folder + 'numbers.txt')
times = loadtxt(Folder + 'walltimes.txt')


if max(error) == 0 or max(numbers) == 0:
	print ("numbers or errors is identically zero!")
	sys.exit()

# We ignore the first few datapoints,
# as they may be underresolved
rate = polyfit(log(numbers[2:]), log(error[2:]), 1)[0]
print ("Error scales with number of cells with order", rate)
plt.loglog(numbers, error, '-o', label='$|u(T)-U_N|$')
plt.loglog(numbers, error[-1]*numbers**rate/(numbers[-1]**rate), '--', label='$\Delta t^{%0.2f}$' % rate)
plt.xlabel('$N$')
plt.ylabel('$Error$')
#plt.xlim([dt[0], dt[1]])
plt.legend(loc=0)
plt.grid()
plt.show()


if max(times) > 0:
	rate = polyfit(log(numbers[2:]), log(times[2:]), 1)[0]
	print ("Runtime scales with number of cells with order", rate)
	plt.loglog(numbers, times, '-o', label='Walltimes')
	plt.loglog(numbers, times[-1]*numbers**rate/(numbers[-1]**rate), '--', label='$\Delta t^{%0.2f}$' % rate)
	plt.xlabel('$N$')
	plt.ylabel('$Time$')
	plt.legend(loc=0)
	plt.grid()
	plt.show()


	rate = polyfit(log(times[2:]), log(error[2:]), 1)[0]
	print ("Error scales with runtime with order", rate)
	plt.loglog(times, error, '-o', label='$|u(T)-U_N|$')
	plt.loglog(times, error[-1]*times**rate/(times[-1]**rate), '--', label='$\Delta t^{%0.2f}$' % rate)
	plt.xlabel('$Time$')
	plt.ylabel('$Error$')
	plt.legend(loc=0)
	plt.grid()
	plt.show()
