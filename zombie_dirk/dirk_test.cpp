#include "writer.hpp"
#include <vector>
#include <iostream>
#include <chrono>
#include "zombieoutbreak.hpp"
#include "dirksolver.hpp"


//----------------mainBegin----------------
int main(int argc, char** argv) {

    double T = 101;
    int N = 1000;
    ZombieOutbreak outbreak(0, 0, 0, 0.03, 0.02);
    std::vector<double> u0(3);
    u0[0] = 500;
    u0[1] = 0;
    u0[2] = 0;

    // Compute the exact solution for the parameters above
    std::vector<double> exact = outbreak.computeExactNoZombies(T, u0[0]);

    // Initialize solver object for the parameters above
    DIRKSolver dirkSolver(outbreak);

    writeToFile("S.txt", u[0]);
    writeToFile("Z.txt", u[1]);
    writeToFile("R.txt", u[2]);
    writeToFile("time.txt", time);

}
//----------------mainEnd----------------
