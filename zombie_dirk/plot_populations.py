from numpy import *
from pylab import *

Folder = 'cmake-build-debug/'

S = loadtxt(Folder + "S.txt")
Z = loadtxt(Folder + "Z.txt")
R = loadtxt(Folder + "R.txt")
t = loadtxt(Folder + 'time.txt')
plot(t,S,label="S")
plot(t,Z,label="Z")
plot(t,R,label="R")

xlabel(r'$Time$')
ylabel(r'$Individuals$')

legend(loc = "best")
show()

