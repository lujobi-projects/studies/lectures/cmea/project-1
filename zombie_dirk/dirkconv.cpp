#include "writer.hpp"
#include <Eigen/Core>
#include <Eigen/LU>
#include <vector>
#include <iostream>
#include <chrono>
#include "zombieoutbreak.hpp"
#include "dirksolver.hpp"


//----------------mainBegin----------------
int main(int argc, char** argv) {

    double T = 101;
    ZombieOutbreak outbreak(0, 0, 0, 0.03, 0.02);
    std::vector<double> u0(3);
    u0[0] = 500;
    u0[1] = 0;
    u0[2] = 0;
    
    // Compute the exact solution for the parameters above
    std::vector<double> exact = outbreak.computeExactNoZombies(T, u0[0]);

    // Initialize solver object for the parameters above
    DIRKSolver dirkSolver(outbreak);

    int minExp = 0;
    int maxExp = 12;
    int countExponents = maxExp - minExp + 1;
    std::vector<double> numbers(countExponents);
    std::vector<double> walltimes(countExponents);
    std::vector<double> errors(countExponents);

    for (int exp = minExp; exp <= maxExp; ++exp) {
        std::cout << "Calculating 200 * 2 ^ " << exp << std::endl;

        int N = 200 * std::pow(2, exp);

        std::vector<double> time(N + 1, 0);
        std::vector<std::vector<double> > u(3, std::vector<double>(N+1, 0));
        u[0][0] = 500;

        dirkSolver.solve(u, time, T, N);

        numbers[exp] = N;
        errors[exp] = 0; // Since i am not allowed to modify the code above to std::vector<double> errors(countExponents, 0);

        for(int i = 0; i < 3; i ++){
            errors[exp] += std::abs(u[i][N] - exact[i]);
        }
    }

    writeToFile("numbers.txt", numbers);
    writeToFile("errors.txt", errors);
    writeToFile("walltimes.txt", walltimes);

}
//----------------mainEnd----------------
